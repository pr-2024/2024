import { Text } from "../../ui/Text";
import Image1 from "../../assets/img/img1.png";
import { CardBlog } from "../../components/Cards";

export const HomePage = () => {
  return (
    <div>
      <Text text={"HOTEL BLOG"} type={"header-h2"} />
      <Text text={"Our News"} type={"header-h1"} />

      <div style={{ display: "flex", justifyContent: "center", columnGap: 24 }}>
        <CardBlog
          image={Image1}
          title={"How To Travel With Map"}
          desc={
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
          }
          linkTo={"/posts/1"}
          countComments={100}
        />
        <CardBlog
          image={Image1}
          title={"How To Travel With Map"}
          desc={
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
          }
          linkTo={"/posts/3"}
          countComments={3}
        />
        <CardBlog
          image={Image1}
          title={"Luxery Twin Room"}
          desc={
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
          }
          linkTo={"/posts/2"}
          countComments={0}
        />
      </div>
    </div>
  );
};
