import { Text } from "../../ui/Text";
import { LinkComponent } from "../../ui/Link";

import IconMap from "../../assets/svg/maps.svg";
import IconComment from "../../assets/svg/comment.svg";

import "./style.scss";

export const CardBlog = ({ image, title, desc, linkTo, countComments = 0 }) => {
  return (
    <div className={"card-blog"}>
      <div className={"card-blog__image"}>
        <img src={image} />
      </div>
      <div className={"card-blog__need-info"}>
        <div className={"card-blog__buttons"}>
          <div className={"card-blog__img"}>
            <img src={IconMap} /> <Text text={"Travel"} type={"text-1"} />
          </div>
          <div>
            <img src={IconComment} />{" "}
            <Text text={`${countComments} Comment`} type={"text-1"} />
          </div>
        </div>
        <div className={"card-blog__title"}>
          <Text text={title} type={"text-2"} />
        </div>
        <div className={"card-blog__desc"}>
          <Text text={desc} type={"text-1"} />
        </div>
        <div className={"card-blog__link"}>
          <LinkComponent label={"Read More"} linkTo={linkTo} />
        </div>
      </div>
    </div>
  );
};
