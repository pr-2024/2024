import classNames from "classnames";
import { Link } from "react-router-dom";
import IconArrowRight from "../../assets/svg/arrow-right.svg";

import "./style.scss";

export const LinkComponent = ({ label, linkTo }) => {
  const baseClassesLink = classNames("base-link");

  return (
    <Link className={baseClassesLink} to={linkTo || "/"}>
      {label} <img src={IconArrowRight} />
    </Link>
  );
};
