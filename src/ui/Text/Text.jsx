import classNames from "classnames";
import "./style.scss";

export const Text = ({ text, type }) => {
  const baseClassesText = classNames("base-text", {
    "base-text__header-h1": type === "header-h1",
    "base-text__header-h2": type === "header-h2",
    "base-text__header-h3": type === "header-h3",
    "base-text__text-1": type === "text-1",
    "base-text__text-2": type === "text-2",
  });

  if (type === "header-h1") {
    return <h1 className={baseClassesText}>{text}</h1>;
  }

  if (type === "header-h2") {
    return <h2 className={baseClassesText}>{text}</h2>;
  }
  if (type === "header-h3") {
    return <h3 className={baseClassesText}>{text}</h3>;
  }

  return <span className={baseClassesText}>{text}</span>;
};
